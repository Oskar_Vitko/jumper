﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float movementSpeed = 17.0f;
    public float jumpForce = 17.0f;
    public float wallSlideSpeed;
    public float wallCheckDistance;

    public bool isGameStarted;

    public Transform wallCheck;

    public LayerMask whatIsWall;

    private float movementDirection;
    private float increasingSpeedKoef = 16.6f;
    private float currentJumpForce;

    private int facingDirection = 1;
    private int limitToIncreasingSpeed = 40;

    private bool isGrounded;
    private bool isTouchingWall;
    private bool isWallSliding;
    private bool canJump;

    private Rigidbody rb;
    
    private void Start()
    {
        currentJumpForce = jumpForce;
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        CheckInput();
        CheckIfCanJump();
        CheckIfWallSliding();
    }
    
    private void FixedUpdate()
    {
        Sliding();
        IncreasingSpeed();
        CheckSurroundings();
    }

    private void CheckSurroundings()
    {
        isTouchingWall = Physics.Raycast(wallCheck.position, transform.right, wallCheckDistance, whatIsWall);
    }

    private void CheckIfCanJump()
    {
        if((isGrounded && rb.velocity.y <= 0) || isWallSliding)
        {
            canJump = true;
        }
        else
        {
            canJump = false;
        }
    }
    
    private void CheckIfWallSliding()
    {
        if(isTouchingWall && !isGrounded)
        {
            isWallSliding = true;
        }
        else
        {
            isWallSliding = false;
        }
    }

    private void CheckInput()
    {
        movementDirection = Input.GetAxisRaw("Horizontal");

        if (Input.GetButtonDown("Jump"))
        {
            StartGame();
            Jump();
        }
    }

    private void Jump()
    {
        if (canJump && !isWallSliding)
        {
            rb.velocity = new Vector3(rb.velocity.x, currentJumpForce);
        }
        else if((isWallSliding || isTouchingWall) && canJump)
        {
            isWallSliding = false;
            rb.velocity = new Vector3(movementSpeed * -facingDirection, currentJumpForce);
            Flip();
        }
    }

    private void StartGame()
    {
        if (isGrounded && canJump)
        {
            int random = Random.Range(0, 2);

            if(random == 0)
            {
                movementDirection = 1;
            }
            else
            {
                movementDirection = -1;
                Flip();
            }

            rb.velocity = new Vector3(movementSpeed * movementDirection, rb.velocity.y);
            isGameStarted = true;
        }
    }

    private void Sliding()
    {
        if (isWallSliding)
        {
            if(rb.velocity.y < -wallSlideSpeed)
            {
                rb.velocity = new Vector3(rb.velocity.x, -wallSlideSpeed);
            }
        }
    }

    private void Flip()
    {
            facingDirection = -facingDirection;
            transform.Rotate(0.0f, 180.0f, 0.0f);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Ground"))
        {
            isGrounded = true;
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.collider.CompareTag("Ground"))
        {
            isGrounded = false;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(wallCheck.position, new Vector3(wallCheck.position.x + wallCheckDistance, wallCheck.position.y, wallCheck.position.z));
    }

    private void IncreasingSpeed()
    {
        if(transform.position.y > limitToIncreasingSpeed)
        {
            currentJumpForce = jumpForce + transform.position.y / increasingSpeedKoef;
        }
    }
}
