﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float cameraSpeed;
    public float catchSpeed;

    public bool isDefeat = false;
    public bool isGameStarted;

    private bool canMove;

    private float limitToStart = 8.0f;
    private float currentSpeed;
    private float diffrenceInY;
    private float upperLimit = 5.0f;
    private float lowerLimit = 10.0f;

    private Transform player;

    private void Start()
    {        
        currentSpeed = cameraSpeed;
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        CheckIfGameStarted();
        CheckIfCanMove();
        CheckIsDefeat();
        CheckUpLimit();
    }

    private void FixedUpdate()
    {
        IncreasingSpeed();
        Move();
    }

    private void CheckIfCanMove()
    {
        if (isDefeat == false && isGameStarted == true)
        {
            canMove = true;
        }
        else
        {
            canMove = false;
        }
    }

    private void CheckUpLimit()
    {
        diffrenceInY = player.position.y - transform.position.y;

        if(diffrenceInY > upperLimit)
        {
            currentSpeed = cameraSpeed + catchSpeed;
        }
        else
        {
            currentSpeed = cameraSpeed;
        }
    }
    
    private void CheckIsDefeat()
    {
        diffrenceInY = transform.position.y - player.position.y;

        if(diffrenceInY > lowerLimit)
        {
            isDefeat = true;
            Application.LoadLevel(Application.loadedLevel);
        }
    }

    private void CheckIfGameStarted()
    {
        if(player.position.y > limitToStart)
        {
            isGameStarted = true;
        }
        else
        {
            isGameStarted = false;
        }
    }

    private void Move()
    {
        if (canMove)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + currentSpeed, transform.position.z);
        }
    }

    private void IncreasingSpeed()
    {
        cameraSpeed = player.position.y / 500;
    }
}
